#!/bin/bash

res="succ"

command_exists() {
    type "$1" &> /dev/null ;
}

# Detect Ubuntu version
detect_ubuntu_version() {
    UBUNTU_MAJOR_VERSION=$(lsb_release -cs)
    ARCH=$(uname -m)    
    if [ $UBUNTU_MAJOR_VERSION == "wily" ]
    then
       ROS_DISTR="indigo"
       if [ $ARCH == "x86_64" ]
       then      
          GAZEBO_SOURCES="http://packages.osrfoundation.org/gazebo/ubuntu-stable"
          ROS_DISTR="kinetic"
          GAZEBO_PKG="gazebo-stable.list"
       else
          echo "Detected a compatible Ubuntu release $UBUNTU_MAJOR_VERSION but incompatible architecture $ARCH"
          exit 0
       fi
    elif [ $UBUNTU_MAJOR_VERSION == "xenial" ]
    then
       ROS_DISTR="kinetic"
       GAZEBO_SOURCES="http://packages.osrfoundation.org/gazebo/ubuntu-stable"
       GAZEBO_PKG="gazebo-stable.list"
    else
       echo "Your Ubuntu version is too old. TAPIR requires Ubuntu 15.10 (Wily) or higher"
       exit 0
    fi    
}

install_common_dependencies() {
    sudo apt-get install build-essential git cmake mercurial pkg-config libboost-all-dev libtinyxml-dev libeigen3-dev libassimp-dev libfcl-dev || res="fail" && return
    sleep 1
}

install_gazebo_dependencies() {
    # First check if we need to install Gazebo at all    
    if command_exists gzclient
    then
        echo "Gazebo is already installed"
    	return   
    fi
    
	# Installs the dependencies for gazebo-7
	sudo sh -c 'echo "deb '$GAZEBO_SOURCES' `lsb_release -cs` main" > /etc/apt/sources.list.d/'$GAZEBO_PKG''
	wget http://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add -
	wget https://bitbucket.org/osrf/release-tools/raw/default/jenkins-scripts/lib/dependencies_archive.sh -O /tmp/dependencies.sh
	sudo apt-get update
	sleep 1
	ROS_DISTRO=$ROS_DISTR GAZEBO_MAJOR_VERSION=7 . /tmp/dependencies.sh 
	sudo apt-get install $(sed 's:\\ ::g' <<< $BASE_DEPENDENCIES) $(sed 's:\\ ::g' <<< $GAZEBO_BASE_DEPENDENCIES)
	sudo apt-get install libsdformat4 libsdformat4-dev libignition-math2-dev libignition-math2 
	sudo apt-get install gazebo7 libgazebo7-dev	
	sleep 1
}

install_ros() {
    # First check if we can run Rviz
    if command_exists rviz
    then
        return
    fi

    # Installs the ros-base variant of ROS plus the visualization stack
    sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
    sudo apt-get update
    sudo apt-get install ros-$ROS_DISTR-ros-base ros-$ROS_DISTR-rviz    
    source /opt/ros/$ROS_DISTR/setup.bash    
}

install_libspatialindex() {
    # Check if libspatialindex is already installed
    siOut=$(ldconfig -p | grep libspatialindex)
    if [ `echo $siOut | grep -c "libspatialindex" ` -gt 0 ]
	then
	    echo "libspatialindex is already installed"
  		return
	fi
    # Downloads, compiles and installs libspatialindex-1.8.5
    wget http://download.osgeo.org/libspatialindex/spatialindex-src-1.8.5.tar.gz
    tar zxfv spatialindex-src-1.8.5.tar.gz
    cd spatialindex-src-1.8.5
    mkdir build && cd build
    cmake -DCMAKE_INSTALL_PREFIX=/usr/local ..
    make -j2 && sudo make install
    cd ../../
    rm -rf spatialindex-src-1.8.5
    rm spatialindex-src-1.8.5.tar.gz
}

install_additional() {    
    sudo apt-get install libsdformat4 libsdformat4-dev libignition-math2-dev libignition-math2 || res="fail" && return
    sleep 1
}

USE_ROS=false
if [ -z $1 ]
then
  echo "Installing without ROS"
else  
  if [ $1 = "--use-ros" ]
  then
      echo "Installing with ROS"
      USE_ROS=true
  else
      echo "Command line option '$1' not recognized. Available option: '--use-ros'"
      return
  fi
fi

detect_ubuntu_version
install_common_dependencies
echo $res
if [ $res != "succ" ]; then
    exit
fi
install_gazebo_dependencies
echo $res
if [ $res != "succ" ]; then
    exit
fi

if [ "$USE_ROS" = true ]
then   
   install_ros
fi

if [ $res != "succ" ]; then
    exit
fi
install_libspatialindex
if [ $res != "succ" ]; then
    exit
fi
echo "Done."

