#!/bin/bash

# Installation script for TAPIR this script installs all the dependencies and builds TAPIR

# Install the dependencies first
wget https://bitbucket.org/hoergems/tapir_scripts/downloads/install_dependencies.sh && chmod +x install_dependencies.sh && ./install_dependencies.sh

# Now we can clone and build TAPIR
wget https://bitbucket.org/hoergems/tapir_scripts/downloads/get_tapir.sh && chmod +x get_tapir.sh && ./get_tapir.sh
